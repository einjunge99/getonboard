module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#FA7676',
        'secondary': '#05C4F4',
      }
    },
  },
  plugins: [],
}