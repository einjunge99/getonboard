# Getting Started with Timer App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Live preview

Go ahead! You can access the app right now on https://getonboard.vercel.app/

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

And...that's it!
