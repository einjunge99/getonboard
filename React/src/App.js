
import React, { useEffect, useState } from 'react'

function App() {
  const [time, setTime] = useState(new Date())

  useEffect(() => {
    const interval = setInterval(() => {
      setTime(new Date())
    }, 1000)

    return () => clearInterval(interval)
  }, [time])

  return (
    <>
      <div class="h-screen">
        <div class="grid justify-items-center items-center h-screen">
          <div>
            <p class="text-4xl text-center font-extralight">
              What time is it?
            </p>
            <div className='blur hover:filter-none cursor-pointer p-8'>
              <p className='text-8xl text-center text-transparent bg-clip-text bg-gradient-to-r from-primary to-secondary'>{time.toLocaleString()} </p>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default App;









